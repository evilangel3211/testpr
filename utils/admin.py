from sqladmin import Admin, ModelView, BaseView
from main import app
from db.base import engine
from db.client import clients
from db.mailing import mailings
from db.tag import tags
from db.message import messages
from db.operator import operators

class ClientAdmin(ModelView, model=clients):
    can_create = True
    column_list = [clients.id, clients.phone]
