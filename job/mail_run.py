from datetime import datetime, timedelta
from typing import List, Optional

from databases.interfaces import Record
from fastapi import Depends
from db.base import database
from repositories.client import ClientRepository
from repositories.base import BaseRepository
from repositories.mailing import MailingRepository
from repositories.message import MessageRepository, MessageIn
from schemas.mail_run import MailRun, MailIn
from db.mail_run import mail_run
from db.client import clients
import requests
import json
from config import API_KEY
from schemas.mailing import Mailing


class MailrunRepository(BaseRepository):

    async def create(self, c: MailIn) -> mail_run:
        mailrun = MailRun(
            mailing_id=c.mailing_id,
            started_at=datetime.now()
        )
        values = {**mailrun.dict()}
        values.pop('id', None)
        query = mail_run.insert().values(**values)
        mailrun.id = await self.database.execute(query)
        return mailrun

    async def get_all(self, limit: int = 100, skip: int = 0) -> List[Record]:
        query = mail_run.select().limit(limit).offset(skip)
        return await self.database.fetch_all(query=query)

    async def get_by_id(self, id: int) -> Optional[MailRun]:
        query = mail_run.select().where(mail_run.c.id == id)
        mailrun = await self.database.fetch_one(query=query)
        if mailrun is None:
            return None
        return MailRun.parse_obj(mailrun)

    def send_msg(self, id: int, phone: int, text: str):
        url = str('https://probe.fbrq.cloud/v1/send/' + str(id))
        Headers = {
            'Authorization': API_KEY,
            'Content-Type': 'text/plain'
        }
        data = {'id': int(id), 'phone': phone, 'text': text}
        payload = json.dumps(data)
        response = requests.request("POST", url, headers=Headers, data=payload)
        return response

    async def start_sending(self, id, mail_id):
        client_repo = ClientRepository(database)
        mailing_repo = MailingRepository(database)
        message_repo = MessageRepository(database)
        client = await client_repo.get_by_id(id=int(id))
        mailing = await mailing_repo.get_by_id(id=int(mail_id))
        phone = int(client.phone)
        text = mailing.text
        response = self.send_msg(id, text=text, phone=phone)
        if response.status_code == 200:
            await message_repo.create(
                MessageIn(sended_at=datetime.now(),
                          status="sent",
                          mailing_id=mail_id,
                          client_id=id)
            )
        else:
            await MessageRepository.create(
                MessageIn(sended_at=datetime.now(),
                          status="not sent",
                          mailing_id=mail_id,
                          client_id=id))

    async def get_today_mail(self):
        query = mail_run.select().where(mail_run.c.started_at > (datetime.today() - timedelta(days=1)))
        todays = await self.database.fetch_all(query=query)
        res = []
        for i in todays:
            res.append(i.mailing_id)
        return res
