from datetime import datetime


from db.base import database
from db.client import clients
from db.mail_run import mail_run
from repositories.client import ClientRepository
from job.mail_run import MailrunRepository
from repositories.mailing import MailingRepository
from schemas.mail_run import MailIn


async def search_for_mailings():
    mail_repo = MailrunRepository(database)
    mailings_repo = MailingRepository(database)
    client_repo = ClientRepository(database)
    already_sent = await mail_repo.get_today_mail()
    mailings = await mailings_repo.get_all()
    for i in mailings:
        if i.start_time <= datetime.today().time() < i.end_time and i.id not in already_sent:
            id_list = await client_repo.get_by_filters(tag_id=i.filter_tag)
            if id_list is not None:
                await mail_repo.create(MailIn(mailing_id=i.id))
                for client in id_list:
                    await mail_repo.start_sending(id=client.id, mail_id=i.id)





