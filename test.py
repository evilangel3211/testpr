from repositories.client import ClientRepository
from schemas.client import ClientIn
from db.client import clients
from typing import List
from db.base import database
import json
import requests
from config import API_KEY


def send_msg( id: int = 1, phone: int = 8999, text: str = 'aaa'):
    url = str('https://probe.fbrq.cloud/v1/send/' + str(id))
    Headers = {
        'Authorization': API_KEY,
        'Content-Type': 'text/plain'
    }
    data = {'id': id, 'phone': phone, 'text': text}
    payload = json.dumps(data)
    response = requests.request("POST", url, headers=Headers, data=payload)
    print(response.json())

send_msg()