from typing import List, Optional
from .base import BaseRepository
from schemas.operator import Operator, OperatorIn
from db.operator import operators


class OperatorRepository(BaseRepository):

    async def create(self, o: OperatorIn) -> Operator:
        operator = Operator(
            code=o.code,
            title=o.title
        )
        values = {**operator.dict()}
        values.pop('id', None)
        query = operators.insert().values(**values)
        operator.id = await self.database.execute(query)
        return operator

    async def update(self, id: int, o: OperatorIn) -> Operator:
        operator = Operator(
            id=id,
            code=o.code,
            title=o.title
        )
        values = {**operator.dict()}
        values.pop('id', None)
        query = operators.update().where(operators.o.id == id).values(**values)
        await self.database.execute(query)
        return operator

    async def get_all(self, limit: int = 100, skip: int = 0) -> List[Operator]:
        query = operators.select().limit(limit).offset(skip)
        return await self.database.fetch_all(query=query)

    async def get_by_id(self, id: int) -> Optional[Operator]:
        query = operators.select().where(operators.m.id == id)
        mailing = await self.database.fetch_one(query=query)
        if mailing is None:
            return None
        return Operator.parse_obj(mailing)