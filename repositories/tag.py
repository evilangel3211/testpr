from typing import List, Optional
from .base import BaseRepository
from schemas.tag import Tag, TagIn
from db.tag import tags


class TagRepository(BaseRepository):

    async def create(self, c: TagIn) -> Tag:
        tag = Tag(
            tag=c.tag
        )
        values = {**tag.dict()}
        values.pop('id', None)
        query = tags.insert().values(**values)
        tag.id = await self.database.execute(query)
        return tag

    async def update(self, id: int, c: TagIn) -> Tag:
        tag = Tag(
            id=id,
            tag=c.tag
        )
        values = {**tag.dict()}
        values.pop('id', None)
        query = tags.update().where(tags.c.id == id).values(**values)
        await self.database.execute(query)
        return tag

    async def get_all(self, limit: int = 100, skip: int = 0) -> List[Tag]:
        query = tags.select().limit(limit).offset(skip)
        return await self.database.fetch_all(query=query)

    async def get_by_id(self, id: int) -> Optional[Tag]:
        query = tags.select().where(tags.c.id == id)
        tag = await self.database.fetch_one(query=query)
        if tag is None:
            return None
        return Tag.parse_obj(tag)