from typing import List, Optional
from .base import BaseRepository
from schemas.mailing import Mailing, MailingIn, FilterMail
from db.mailing import mailings


class MailingRepository(BaseRepository):

    async def create(self, c: MailingIn) -> Mailing:
        mailing = Mailing(
            start_time=c.start_time,
            text=c.text,
            filter_tag=c.filter_tag,
            filter_operator=c.filter_operator,
            end_time=c.end_time
        )
        values = {**mailing.dict()}
        values.pop('id', None)
        query = mailings.insert().values(**values)
        mailing.id = await self.database.execute(query)
        return mailing

    async def update(self, id: int, c: MailingIn) -> Mailing:
        mailing = Mailing(
            id=id,
            start_time=c.start_time,
            text=c.text,
            filter_tag=c.filter_tag,
            filter_operator=c.filter_operator,
            end_time=c.end_time
        )
        values = {**mailing.dict()}
        values.pop('id', None)
        query = mailings.update().where(mailings.c.id == id).values(**values)
        await self.database.execute(query)
        return mailing

    async def get_all(self, limit: int = 100, skip: int = 0) -> List[Mailing]:
        query = mailings.select().limit(limit).offset(skip)
        return await self.database.fetch_all(query=query)

    async def get_by_id(self, id: int) -> Optional[Mailing]:
        query = mailings.select().where(mailings.c.id == id)
        mailing = await self.database.fetch_one(query=query)
        if mailing is None:
            return None
        return Mailing.parse_obj(mailing)

    async def get_tag_operator_id(self, mail_id) -> FilterMail:
        query = mailings.select().where(mailings.c.id == mail_id)
        filters = await self.database.fetch_all(query=query)
        return FilterMail.parse_obj(filters)


