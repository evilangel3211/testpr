from datetime import datetime
from typing import List, Optional

from .base import BaseRepository
from schemas.client import Client, ClientIn
from db.client import clients


class ClientRepository(BaseRepository):

    async def create(self, c: ClientIn) -> Client:
        client = Client(
            phone=c.phone,
            tag_id=c.tag_id,
            timezone=c.timezone,
            operator_id=c.operator_id
        )
        values = {**client.dict()}
        values.pop('id', None)
        query = clients.insert().values(**values)
        client.id = await self.database.execute(query)
        return client

    async def update(self, id: int, c: ClientIn) -> Client:
        client = Client(
            id=id,
            phone=c.phone,
            tag_id=c.tag_id,
            timezone=c.timezone,
            operator_id=c.operator_id
        )
        values = {**client.dict()}
        values.pop('id', None)
        values.pop('timezone', None)
        query = clients.update().where(clients.c.id == id).values(**values)
        await self.database.execute(query)
        return client

    async def get_all(self, limit: int = 100, skip: int = 0) -> List[Client]:
        query = clients.select().limit(limit).offset(skip)
        return await self.database.fetch_all(query=query)

    async def get_by_id(self, id: int) -> Optional[Client]:
        query = clients.select().where(clients.c.id == id)
        client = await self.database.fetch_one(query=query)
        if client is None:
            return None
        return Client.parse_obj(client)

    async def get_by_filters(self, tag_id: int) -> List[Client]:
        query = clients.select().where(clients.c.tag_id == tag_id)
        return await self.database.fetch_all(query=query)
        if id_list is None:
            return None
        return Client.parse_obj(id_list)

