from typing import List, Optional
from .base import BaseRepository
from schemas.message import Message, MessageIn
from db.message import messages


class MessageRepository(BaseRepository):

    async def create(self, m: MessageIn) -> Message:
        message = Message(
            sended_at=m.sended_at,
            status=m.status,
            mailing_id=m.mailing_id,
            client_id=m.client_id,
        )
        values = {**message.dict()}
        values.pop('id', None)
        query = messages.insert().values(**values)
        message.id = await self.database.execute(query)
        return message

    async def update(self, id: int, m: MessageIn) -> Message:
        message = Message(
            id=id,
            sended_at=m.sended_at,
            status=m.status,
            mailing_id=m.mailing_id,
            client_id=m.client_id,
        )
        values = {**message.dict()}
        values.pop('id', None)
        query = message.update().where(message.m.id == id).values(**values)
        await self.database.execute(query)
        return message

    async def get_all(self, limit: int = 100, skip: int = 0) -> List[Message]:
        query = messages.select().limit(limit).offset(skip)
        return await self.database.fetch_all(query=query)

    async def get_by_id(self, id: int) -> Optional[Message]:
        query = messages.select().where(messages.m.id == id)
        message = await self.database.fetch_one(query=query)
        if message is None:
            return None
        return Message.parse_obj(message)