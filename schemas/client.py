from pydantic import BaseModel, validator
from typing import Optional


class Client(BaseModel):
    id: Optional[str] = None
    phone: str
    operator_id: Optional[int] = None
    tag_id: Optional[int] = None
    timezone: str


class ClientIn(BaseModel):
    phone: str
    operator_id: Optional[int] = None
    tag_id: Optional[int] = None
    timezone: str

