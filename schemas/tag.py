import datetime
from typing import Optional
from pydantic import BaseModel


class Tag(BaseModel):
    id: Optional[str] = None
    tag: str


class TagIn(BaseModel):
    tag: str


