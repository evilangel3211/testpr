import datetime

from pydantic import BaseModel
from typing import Optional


class MessageIn(BaseModel):
    sended_at: datetime.datetime
    status: str
    mailing_id: Optional[int]
    client_id: Optional[int]


class Message(BaseModel):
    id: Optional[str] = None
    sended_at: datetime.datetime
    status: str
    mailing_id: Optional[int]
    client_id: Optional[int]
