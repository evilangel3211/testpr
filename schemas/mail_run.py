import datetime


from pydantic import BaseModel
from typing import Optional


class MailRun(BaseModel):
    id: Optional[int]
    mailing_id: int
    started_at: datetime.datetime


class MailIn(BaseModel):
    mailing_id: int