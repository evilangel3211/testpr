from pydantic import BaseModel
from typing import Optional, List

class OperatorIn(BaseModel):
    code: list
    title: str


class Operator(BaseModel):
    id: Optional[str] = None
    code: list
    title: str