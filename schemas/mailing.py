from datetime import time

from typing import List
from pydantic import BaseModel
from typing import Optional


class Mailing(BaseModel):
    id: Optional[str] = None
    start_time: time
    text: str
    filter_tag: Optional[int]
    filter_operator: Optional[int]
    end_time: time


class MailingIn(BaseModel):
    start_time: time
    text: str
    filter_tag: Optional[int]
    filter_operator: Optional[int]
    end_time: time


class FilterMail(BaseModel):
    filter_tag: Optional[int]
    filter_operator: Optional[int]

