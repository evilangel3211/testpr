from fastapi import APIRouter, Depends, HTTPException, status
from typing import List
from schemas.message import Message, MessageIn
from repositories.message import MessageRepository
from .depends import get_message_repository


router = APIRouter()


@router.get('/', response_model=List[Message])
async def read_messages(
        messages: MessageRepository = Depends(get_message_repository),
        limit: int = 100,
        skip: int = 0):
    return await messages.get_all(limit=limit, skip=skip)


@router.post('/', response_model=Message)
async def create(
        message: MessageIn,
        messages: MessageRepository = Depends(get_message_repository)):
    return await messages.create(m=message)


@router.patch('/', response_model=Message)
async def update_message(
        id: int,
        message: MessageIn,
        messages: MessageRepository = Depends(get_message_repository)):
    old_mailing = await messages.get_by_id(id=id)
    if old_mailing is None:
        return HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Not found message')
    return await messages.update(id=id, m=message)