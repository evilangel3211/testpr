from fastapi import Depends, HTTPException, status
from repositories.client import ClientRepository
from repositories.mailing import MailingRepository
from repositories.message import MessageRepository
from repositories.operator import OperatorRepository
from repositories.tag import TagRepository
from job.mail_run import MailrunRepository
from db.base import database


def get_client_repository() -> ClientRepository:
    return ClientRepository(database)


def get_mailing_repository() -> MailingRepository:
    return MailingRepository(database)


def get_message_repository() -> MessageRepository:
    return MessageRepository(database)


def get_operator_repository() -> OperatorRepository:
    return OperatorRepository(database)


def get_tag_repository() -> TagRepository:
    return TagRepository(database)


def get_mailrun_repository() -> MailrunRepository:
    return MailrunRepository(database)
