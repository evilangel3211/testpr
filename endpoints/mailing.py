from fastapi import APIRouter, Depends, HTTPException, status
from typing import List
from schemas.mailing import Mailing, MailingIn
from repositories.mailing import MailingRepository
from .depends import get_mailing_repository


router = APIRouter()


@router.get('/', response_model=List[Mailing])
async def read_mailings(
        mailings: MailingRepository = Depends(get_mailing_repository),
        limit: int = 100,
        skip: int = 0):
    return await mailings.get_all(limit=limit, skip=skip)


@router.post('/', response_model=Mailing)
async def create(
        mailing: MailingIn,
        mailings: MailingRepository = Depends(get_mailing_repository)):
    return await mailings.create(c=mailing)


@router.patch('/', response_model=Mailing)
async def update_mailings(
        id: int,
        mailing: MailingIn,
        mailings: MailingRepository = Depends(get_mailing_repository)):
    old_mailing = await mailings.get_by_id(id=id)
    if old_mailing is None:
        return HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Not found mailing')
    return await mailings.update(id=id, m=mailing)