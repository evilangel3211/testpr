from fastapi import APIRouter, Depends, HTTPException, status
from typing import List
from schemas.client import Client, ClientIn
from repositories.client import ClientRepository
from endpoints.depends import get_client_repository

router = APIRouter()


@router.get('/', response_model=List[Client])
async def read_clients(
        clients: ClientRepository = Depends(get_client_repository),
        limit: int = 100,
        skip: int = 0):
    return await clients.get_all(limit=limit,skip=skip)


@router.post('/', response_model=Client)
async def create(
        client: ClientIn,
        clients: ClientRepository = Depends(get_client_repository)):
    return await clients.create(c=client)


@router.patch('/', response_model=Client)
async def update_client(
        id: int,
        client: ClientIn,
        clients: ClientRepository = Depends(get_client_repository)):
    old_client = await clients.get_by_id(id=id)
    if old_client is None:
        return HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Not found user')
    return await clients.update(id=id, c=client)
