from fastapi import APIRouter, Depends, HTTPException, status
from typing import List
from schemas.tag import Tag, TagIn
from repositories.tag import TagRepository
from .depends import get_tag_repository


router = APIRouter()


@router.get('/', response_model=List[Tag])
async def read_tags(
        tags: TagRepository = Depends(get_tag_repository),
        limit: int = 100,
        skip: int = 0):
    return await tags.get_all(limit=limit, skip=skip)


@router.post('/', response_model=Tag)
async def create(
        tag: TagIn,
        tags: TagRepository = Depends(get_tag_repository)):
    return await tags.create(c=tag)


@router.patch('/', response_model=Tag)
async def update_tag(
        id: int,
        tag: TagIn,
        tags: TagRepository = Depends(get_tag_repository)):
    old_tag = await tags.get_by_id(id=id)
    if old_tag is None:
        return HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Not found tag')
    return await tags.update(id=id, c=tag)