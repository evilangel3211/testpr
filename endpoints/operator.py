from fastapi import APIRouter, Depends, HTTPException, status
from typing import List
from schemas.operator import Operator, OperatorIn
from repositories.operator import OperatorRepository
from .depends import get_operator_repository


router = APIRouter()


@router.get('/', response_model=List[Operator])
async def read_operators(
        operators: OperatorRepository = Depends(get_operator_repository),
        limit: int = 100,
        skip: int = 0):
    return await operators.get_all(limit=limit, skip=skip)


@router.post('/', response_model=Operator)
async def create(
        operator: OperatorIn,
        operators: OperatorRepository = Depends(get_operator_repository)):
    return await operators.create(o=operator)


@router.patch('/', response_model=Operator)
async def update_operator(
        id: int,
        operator: OperatorIn,
        operators: OperatorRepository = Depends(get_operator_repository)):
    old_mailing = await operators.get_by_id(id=id)
    if old_mailing is None:
        return HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Not found operator')
    return await operators.update(id=id, o=operator)