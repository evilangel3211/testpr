from . import client
from . import mailing
from . import message
from . import operator
from . import tag
from . import mail_run
from .base import metadata, engine


metadata.create_all(bind=engine)