import datetime

import sqlalchemy
from sqlalchemy import Table, Column, String, Integer, ARRAY, DateTime
from .base import metadata

mail_run = Table(
    'mail_run',
    metadata,
    Column('id', Integer, primary_key=True, autoincrement=True, unique=True),
    Column('mailing_id', Integer, sqlalchemy.ForeignKey('mailings.id'), nullable=False),
    Column('started_at', DateTime, default=datetime.datetime.now())
)