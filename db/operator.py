from sqlalchemy import Table, Column, String, Integer, ARRAY
from .base import metadata

operators = Table(
    'operators',
    metadata,
    Column('id', Integer, primary_key=True, autoincrement=True, unique=True),
    Column('code', ARRAY(String)),
    Column('title', String, unique=True)
)
