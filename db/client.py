import sqlalchemy
from sqlalchemy import Table, Column, String, Integer
from .base import metadata

clients = Table(
    'clients',
    metadata,
    Column('id', Integer, primary_key=True, autoincrement=True, unique=True),
    Column('phone', String, unique=True),
    Column('operator_id', Integer,
           sqlalchemy.ForeignKey('operators.id'),
           nullable=True, default=None),
    Column('tag_id', Integer,
           sqlalchemy.ForeignKey('tags.id'),
           nullable=True, default=None),
    Column('timezone', String, default='Asia/Yekaterinburg')
)


