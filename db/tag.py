import sqlalchemy
from sqlalchemy import Table, Column, Integer, String
from .base import metadata

tags = Table(
    'tags',
    metadata,
    Column('id', Integer, primary_key=True, autoincrement=True, unique=True),
    Column('tag', String, unique=True)
)