from datetime import datetime
import sqlalchemy
from sqlalchemy import Table, Column, String, Integer, DateTime, Time, func
from .base import metadata


mailings = Table(
    'mailings',
    metadata,
    Column('id', Integer, primary_key=True, autoincrement=True, unique=True),
    Column('start_time', Time, default=str(datetime.strftime(datetime.now(),"%H:%M:%S"))),
    Column('text', String),
    Column('filter_tag', Integer,
           sqlalchemy.ForeignKey('tags.id'),
           nullable=True, default=None),
    Column('filter_operator', Integer,
           sqlalchemy.ForeignKey('operators.id'),
           nullable=True, default=None),
    Column('end_time', Time, default=str(datetime.strftime(datetime.now(), "%H:%M:%S")))
)