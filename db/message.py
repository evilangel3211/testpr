import sqlalchemy
from sqlalchemy import Table, Column, String, Integer, DateTime
from .base import metadata
import datetime

messages = Table(
    'messages',
    metadata,
    Column('id', Integer, primary_key=True, autoincrement=True, unique=True),
    Column('sended_at', DateTime, default=datetime.datetime.utcnow()),
    Column('status', String),
    Column('mailing_id', Integer,
           sqlalchemy.ForeignKey('mailings.id'),
           nullable=True, default=None),
    Column('client_id', Integer,
           sqlalchemy.ForeignKey('clients.id'),
           nullable=True, default=None)
)