import uvicorn
from fastapi import FastAPI
from fastapi_utils.tasks import repeat_every
from prometheus_fastapi_instrumentator import Instrumentator


from db.base import database
from endpoints import client, mailing, message, operator, tag
from job.mail_run import MailrunRepository
from job.task import search_for_mailings
from repositories.base import BaseRepository

app = FastAPI(title='Mailing')

app.include_router(client.router, prefix='/clients', tags=['clients'])
app.include_router(mailing.router, prefix='/mailings', tags=['mailings'])
app.include_router(message.router, prefix='/messages', tags=['messages'])
app.include_router(operator.router, prefix='/operators', tags=['operators'])
app.include_router(tag.router, prefix='/tags', tags=['tags'])


@app.on_event('startup')
async def startup():
    await database.connect()
    Instrumentator().instrument(app).expose(app)


@app.on_event('startup')
@repeat_every(seconds=1, raise_exceptions=True)
async def start_mail():
    print('Working')
    await search_for_mailings()


@app.on_event('shutdown')
async def shutdown():
    await database.disconnect()


if __name__ == '__main__':
    uvicorn.run('main:app', port=8000, host='0.0.0.0', reload=True)
